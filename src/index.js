import './css/index.css'
import './less/index.less'
import './scss/index.scss'
import Cookies from 'js-cookie'

const contactBtn = document.getElementById('contactBtn')

contactBtn.addEventListener('click', setCookiesHandler)

function setCookiesHandler() {
	let inFiveMinutes = new Date(new Date().getTime() + 5 * 60 * 1000)

	Cookies.set('experiment', 'novalue', {
		expires: inFiveMinutes
	})

	!Cookies.get('new-user') ? Cookies.set('new-user', true) : Cookies.set('new-user', false)

	console.log(document.cookie)
}

